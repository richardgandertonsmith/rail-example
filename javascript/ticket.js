/* stations and destinations

I have setup the data to represent an api return
and will allow the 'system' to be flexible so if more stations or destinations are add then
it would be able to handle this

*/

var stations = {

    'Bristol Temple Meads': [
        {'name': 'London Paddington','distance': 104},
        {'name': 'Birmingham New Street', 'distance': 78}
    ],

    'London Paddington': [
        {'name': 'Birmingham New Street', 'distance': 78},
        {'name': 'Cambridge', 'distance': 49}
    ],
    'Cambridge': [
        {'name':'London Paddington', 'distance': 49},
        {'name':'Birmingham New Street', 'distance': 104}
    ],
    'Birmingham New Street': [
        {'name': 'Cambridge', 'distance': 104},
        {'name': 'Birstol Temple Meads', 'distance': 78}
    ]
}

/* Constant DATA */
const PENCE_PER_MILE = 80;
const CHILD = 0.5;
const RAILCARD = 0.33;
const MAX_TICKETS = 8;

/* add variables that will handle costing for journey */
var distance = 0;
var destination = '';
var noTickets = 0;

/** read the incoming data and return all departure stations */
function getStations() {
    let myStations = [];
    myStations.push(Object.keys(stations));
    return myStations;
}

/* get all possible destinations for selected station */
function getDestinations(stationName) {
    let myDests = [];
    for (dest in stations[stationName]) {
        myDests.push( [ stations[stationName][dest]['name'], stations[stationName][dest]['distance'] ] );
    }
    return myDests;
}

/** setup some actions to display data to the screen */
function displayStations(stations) {
    let stationHTML = '';
    for (i = 0; i < stations[0].length; i++) {
        stationHTML += '<div class="departure" data-stationId="' + stations[0][i] + '">' + stations[0][i] + '</div>';
    }
    return stationHTML
}


function displayDestinations(destinations) {
    let destinationHTML = '';
    for (i = 0; i < destinations.length; i++) {
        let distance = destinations[i][1];
        let name = destinations[i][0];
        destinationHTML += '<div class="destination" data-distance="' + distance + '">' + name + '</div>';
    }
    return destinationHTML;
}

/* Display ticket control for adding tickets and displaying total cost */
function displayTicketsControl() {
    var ticketControl =
        '<div>' +
        '<div class="addTicket" style="display:inline-block" data-ticket="Adult">Add Adult Ticket</div>'+
        '<div class="addTicket" style="display:inline-block" data-ticket="Child">Add Child Ticket</div>'+
        '<div class="addTicket" style="display:inline-block"><span>Total </span> <span id="totals"></span></div>'+
        '</div>';
return ticketControl;
}

/** display ticket to the screen, show adult, child ticket and option to travel with railcard */
function displayTicket(type, railcard) {
    let cost = getCostOfTicket(type, railcard);
    let ticket = '<div class="ticket" data-type="'+type+'" data-railcard="'+railcard+'">'+type+' Ticket <span class="cost" style="margin-left: 50px;">'+cost+'</span><span class="railcard" style="margin-left: 150px;"><input type="checkbox">Add Railcard</span>  <span class="close" style="margin-left: 30px; color: red;"><i class="fas fa-times"></i></span></div>';
    return ticket;
}

/* this will be used to get the cost of a ticket */
function getCostOfTicket(type, railcard) {
    let cost = 0;
    if (type === 'Adult') {
        cost = (distance * PENCE_PER_MILE);
    }
    if (type === 'Child') {
        cost = (distance * PENCE_PER_MILE) * CHILD;
    }
    if (railcard === 'yes') {
        cost = (distance * PENCE_PER_MILE) * RAILCARD;
    }
    return ticketCost(cost);
    // return ((Math.round(cost / 10) * 10) / 100).toFixed(2);
}

/** accepts pence value
 *
 * returns pounds charge rounded to 10p
 */
function ticketCost(cost) {
    return ((Math.round(cost / 10) * 10) / 100).toFixed(2);
}

/* get total cost for tickets bought */
function getTotalCost() {
    let total = 0;
    $('#tickets > .ticket').each(function () {
        total += (parseFloat($(this).find('.cost').text()) * 100);
    });
    total = ticketCost(total);
    $('#totals').text(total);
}

/** setup some actions to display data and respond to user selection */
// run on page load
$(document).ready(function() {
    let myStations = getStations();
    let startStations = displayStations(myStations);
    $('#stations').append(startStations);
});
// get destinations on stations selection
$(document).on('click', '#stations div.departure', function(){
    $('#destinations').empty();
    $('#tickets-heading').empty();
    $('#tickets').empty();
    $('div.departure').css('background-color','#f1f1f1');
    $(this).css('background-color','#090');
    let stationId = $(this).attr('data-stationId');
    let destinations = getDestinations(stationId);
    let destHTML = displayDestinations(destinations);
    $('#destinations').append(destHTML);
});

/* get ticket control once destination is selected */
$(document).on('click', 'div.destination', function(){
    $('#tickets').empty();
    $('div.destination').css('background-color','#f1f1f1');
    $(this).css('background-color','#090');
    distance = $(this).attr('data-distance');
    $('#tickets-heading').empty();
    let ticketControl = displayTicketsControl();
    $('#tickets-heading').append(ticketControl);
});

/** add ticket to be purchased */
$(document).on('click', '.addTicket', function(){
    if (noTickets < MAX_TICKETS) {
        let ticketType = $(this).attr('data-ticket');
        let ticket = displayTicket(ticketType, 'no');
        $('#tickets').append(ticket);
        noTickets++;
    } else {
        alert('Maximum ' + MAX_TICKETS + ' tickets can be bought');
    }
    getTotalCost();
});

/* add controls for removing a ticket */
$(document).on('click', '.close', function(){
    $(this).closest('.ticket').remove();
    noTickets--;
    getTotalCost();
});

/* using a railcard with this ticket */
$(document).on('change', 'input[type=checkbox]', function(){
    let ticket = $(this).closest('.ticket');
    var railcard = ticket.attr('data-railcard');
    if (railcard == 'no') {
        ticket.attr('data-railcard', 'yes');
    } else {
        ticket.attr('data-railcard', 'no');
    }
    let cost = getCostOfTicket(ticket.attr('data-type'), ticket.attr('data-railcard'));
    ticket.find('.cost').text(cost);
    getTotalCost();
});


